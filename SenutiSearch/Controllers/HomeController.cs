﻿using System;
using System.Diagnostics;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using SenutiSearch.Models;
using SenutiSearch.Services;
using SenutiSearch.ViewModels;

namespace SenutiSearch.Controllers
{
    public class HomeController : Controller
    {
        private readonly IClickDataStore _clickDataStore;
        private readonly IClickEventStore _clickEventStore;
        private readonly IConfiguration _config;
        private readonly ILogger<HomeController> _logger;

        public HomeController(IConfiguration config, ILogger<HomeController> logger, IClickDataStore clickDataStore, IClickEventStore clickEventStore)
        {
            _logger = logger;
            _clickDataStore = clickDataStore;
            _clickEventStore = clickEventStore;
            _config = config;
        }

        [HttpGet]
        public IActionResult Index()
        {
            return View();
        }

        [HttpPost, Route("")]
        public IActionResult IndexPost(string term, string isExplicit, int limit = 25)
        {
            return Redirect($"/itunes?term={term}&explicit={isExplicit}&limit={limit}&country=US");
        }

        [HttpGet, Route("itunes")]
        public async Task<IActionResult> Itunes(string term, string isExplicit, int limit)
        {
            using HttpClient client = new HttpClient();
            try
            {
                // TODO: make country and media type dymanic
                using HttpResponseMessage response = await client.GetAsync(new Uri($"https://itunes.apple.com/search?term={term}&explicit={isExplicit}&limit={limit}&country=US"));
                response.EnsureSuccessStatusCode();

                IConfigurationSection ItunesPartnerTokens = _config.GetSection("ItunesPartnerTokens");

                var resultsViewModel = new ResultsViewModel
                {
                    ApiResponse = JsonConvert.DeserializeObject<ItunesApiResponse>(await response.Content.ReadAsStringAsync()),
                    CampaignToken = ItunesPartnerTokens.GetValue<string>("CampaignToken"),
                    PartnerToken = ItunesPartnerTokens.GetValue<string>("PartnerToken"),
                    SearchTerm = term
                };

                return View(resultsViewModel);
            }
            catch (HttpRequestException httpRequestException)
            {
                return BadRequest($"Error getting results from Itunes: {httpRequestException.Message}");
            }
        }

        [HttpGet, Route("recordclick")]
        public async Task<IActionResult> TrackClick(int trackId, int mozInputSource, int pageX, int pageY, string searchTerm, string timeClicked, string userIp, string userAgent)
        {
            var id = Guid.NewGuid().ToString();
            try
            {
                await _clickDataStore.RecordClickAsync(trackId);
                await _clickEventStore.RecordClickEventAsync(id, mozInputSource, pageX, pageX, searchTerm, timeClicked, trackId, userIp, userAgent);

                return Ok();
            }
            catch
            {
                return Error();
            }
        }

        [HttpGet, Route("clickcount")]
        public async Task<IActionResult> ClickCount()
        {
            ClickCountViewModel clickCountViewModel = new ClickCountViewModel
            {
                TrackedClicks = await _clickDataStore.LoadClicksAsync(),
                TrackedClickEvents = await _clickEventStore.LoadClickEventsAsync()
            };

            return View(clickCountViewModel);
        } 

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
