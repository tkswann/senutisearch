﻿using System;
using System.Collections.Generic;
using SenutiSearch.Models;

namespace SenutiSearch.ViewModels
{
    public class ClickCountViewModel
    {
        public IList<TrackedClick> TrackedClicks { get; set; }
        public IList<ClickEvent> TrackedClickEvents { get; set; }
    }
}
