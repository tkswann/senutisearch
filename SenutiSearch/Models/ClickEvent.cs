﻿using System;
namespace SenutiSearch.Models
{
    public class ClickEvent
    {
        // FIXME: WARNING MozInputSource is non-standard and experimental
        // should  not be used in production. Use at own risk
        // https://developer.mozilla.org/en-US/docs/Web/API/MouseEvent/mozInputSource
        public string Id { get; set; }
        public int MozInputSource { get; set; }
        public int PageX { get; set; }
        public int PageY { get; set; }
        public string SearchTerm { get; set; }
        public string TimeClicked { get; set; }
        public int TrackId { get; set; }
        public string UserIp { get; set; }
        public string UserAgent { get; set; }
    }
}
