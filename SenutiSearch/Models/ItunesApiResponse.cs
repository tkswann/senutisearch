﻿using System;
using System.Collections.Generic;

namespace SenutiSearch.Models
{
    public class ItunesApiResponse
    {
        public int ResultCount { get; set; }
        public List<ItunesResult> Results { get; set; }
    }
}
