﻿namespace SenutiSearch.Models
{
    public class ItunesResult
    {
        public string ArtistName { get; set; }
        public string ArtworkUrl100 { get; set; }
        public string CollectionName { get; set; }
        public string Kind { get; set; }
        public string LongDescription { get; set; }
        public string PrimaryGenreName { get; set; }
        public int TrackId { get; set; }
        public string TrackName { get; set; }
        public string TrackViewUrl { get; set; }
    }
}
