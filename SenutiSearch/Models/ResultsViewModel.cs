﻿using System;
using System.Collections.Generic;
using SenutiSearch.Models;

namespace SenutiSearch.ViewModels
{
    public class ResultsViewModel
    {
        public ItunesApiResponse ApiResponse { get; set; }
        public string CampaignToken { get; set; }
        public string PartnerToken { get; set; }
        public string SearchTerm { get; set; }
    }
}
