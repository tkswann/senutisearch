﻿using System;
using System.ComponentModel.DataAnnotations;

namespace SenutiSearch.Models
{
    public class Search
    {
        [StringLength(3, MinimumLength = 2)]
        [Required]
        public string IsExplicit { get; set; }
        [Range(1, 200)]
        public int Limit { get; set; }
        [StringLength(250)]
        [Required]
        public string Term { get; set; }
    }
}
