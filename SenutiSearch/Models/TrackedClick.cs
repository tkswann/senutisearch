﻿namespace SenutiSearch.Models
{
    public class TrackedClick
    {
        public int Id { get; set; }
        public int Clicks { get; set; }
    }
}
