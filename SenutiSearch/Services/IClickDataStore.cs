﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using SenutiSearch.Models;

namespace SenutiSearch.Services
{
    public interface IClickDataStore
    {
        Task RecordClickAsync(int id);
        Task<IList<TrackedClick>> LoadClicksAsync();
        Task<TrackedClick> LoadClickAsync(int id);
    }
}
