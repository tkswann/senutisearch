﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using SenutiSearch.Models;

namespace SenutiSearch.Services
{
    public interface IClickEventStore
    {
        Task RecordClickEventAsync(string id, int mozInputSource, int pageX, int pageY, string searchTerm, string timeClicked, int trackId, string userIp, string userAgent);
        Task<IList<ClickEvent>> LoadClickEventsAsync();
        Task<ClickEvent> LoadClickEventAsync(string id);
    }
}
