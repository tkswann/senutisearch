﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SenutiSearch.Models;

namespace SenutiSearch.Services
{
    public class InMemoryClickDataStore : IClickDataStore
    {
        private readonly IList<TrackedClick> trackedClicks;

        public InMemoryClickDataStore()
        {
            trackedClicks = new List<TrackedClick>();
        }

        public Task<TrackedClick> LoadClickAsync(int id)
        {
            return Task.FromResult(trackedClicks.FirstOrDefault(c => c.Id == id));
        }

        public Task<IList<TrackedClick>> LoadClicksAsync()
        {
            return Task.FromResult(trackedClicks);
        }

        public Task RecordClickAsync(int id)
        {
            var click = trackedClicks.FirstOrDefault(c => c.Id == id);

            if (click == null)
            {
                click = new TrackedClick
                {
                    Id = id,
                };
                trackedClicks.Add(click);
            }

            click.Clicks++;

            return Task.CompletedTask;
        }
    }
}
