﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SenutiSearch.Models;

namespace SenutiSearch.Services
{
    public class InMemoryClickEventStore : IClickEventStore
    {
        private readonly IList<ClickEvent> trackedClickEvents;

        public InMemoryClickEventStore()
        {
            trackedClickEvents = new List<ClickEvent>();
        }

        public Task<ClickEvent> LoadClickEventAsync(string id)
        {
            return Task.FromResult(trackedClickEvents.FirstOrDefault(c => c.Id == id));
        }

        public Task<IList<ClickEvent>> LoadClickEventsAsync()
        {
            return Task.FromResult(trackedClickEvents);
        }

        public Task RecordClickEventAsync(string id, int mozInputSource, int pageX, int pageY, string searchTerm, string timeClicked, int trackId, string userIp, string userAgent)
        {
            var clickEvent = trackedClickEvents.FirstOrDefault(c => c.Id == id);

            if (clickEvent == null)
            {
                clickEvent = new ClickEvent
                {
                    Id = id,
                    MozInputSource = mozInputSource,
                    PageX = pageX,
                    PageY = pageY,
                    SearchTerm = searchTerm,
                    TimeClicked = timeClicked,
                    TrackId = trackId,
                    UserIp = userIp,
                    UserAgent = userAgent
                };
                trackedClickEvents.Add(clickEvent);
            }

            return Task.CompletedTask;
        }
    }
}
